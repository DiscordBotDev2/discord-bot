echo "--------------------------------------------------------------------------------"
echo "Welcome to the build script!"
echo "This script will automatically install the necesarry dependencies."
echo "--------------------------------------------------------------------------------"
echo "PRESS ANY KEY TO CONTINUE..."
read
echo "--------------------------------------------------------------------------------"
echo "To run any of the code provided you will be required to install" 
echo "some dependencies and the python programming language."
echo "--------------------------------------------------------------------------------"
echo 
echo "--------------------------------------------------------------------------------"
echo "First go to https://python.org and install python"
echo "after you have done it press enter and the script will install the"
echo "required dependencies(if you prefer installing the dependencies"
echo "yourself check scripts.txt in the doc folder for the commands the scripts will"
echo "execute if using content from the internet and on this script press"
echo "n)."
echo ---------------------------------------------------------------------------------
while true; do
    read -p "Do you wish to install the dependencies(Y/n)?" yn
    case $yn in
        [Yy]* ) pip install discord.py; break;;
        [Nn]* ) exit;;
        * ) pip freeze; break;;
    esac
done
echo "Thank you for using the build script!"
echo "Now run the start.py script to start designing the discord bot"