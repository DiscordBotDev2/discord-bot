# To contribute:
1. Clone the repository and make changes,
2. Submit a pull request,
3. Wait until I respond,
4. If necessary make some changes to your code,
5. Complete.
OR if you just want a issue to be fixed and you don't want to edit <br>
the code submit an issue and wait a little while longer as pull requests have <br>
priority over issues.
